public View getView(int position, View convertView, ViewGroup parent) {
	View item = mInflater.inflate(R.layout.list_item_icon_text, null);
	((TextView) item.findViewById(R.id.text)).setText(DATA[position]);
	((ImageView) item.findViewById(R.id.icon))
			.setImageBitmap((position & 1) == 1 ? mIcon1 : mIcon2);
	return item;
}