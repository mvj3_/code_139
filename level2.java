public View getView(int position, View convertView, ViewGroup parent) {
	if (convertView == null) {
		convertView = mInflater.inflate(R.layout.item, null);
	}
	((TextView) convertView.findViewById(R.id.text))
			.setText(DATA[position]);
	((ImageView) convertView.findViewById(R.id.icon))
			.setImageBitmap((position & 1) == 1 ? mIcon1 : mIcon2);
	return convertView;
}